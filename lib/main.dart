//Name: Carlitos Carmona
//Email: carmo025@cougars.csusm.edu
//Professor: Zampell
//Class: CS 481 T/TH 7:00pm-8:15pm
//HW3 - ADD ON TAP FUNCTIONALITY USING STATES
//My app demonstrates my love for fishing and the benefits
//of going out to sea. I created a toggle on and off for my
//heart icon. I hope you like fishing too!

/*Bitbucket
https://bitbucket.org/Carmo025/cs481_hw3/src/master/
*/

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

//STARTING POINT ABSOLUTE MINIMUM TO RUN
void main()
{
  runApp(MyApp());
}

//CONTAINER -> ROW -> - COLUMN -> BRANCHES-> ICON or -> NEW CONTAINER -> TEXT
class MyApp extends StatelessWidget {
  //INITIALIZES CONTAINER
  Widget titleSection = Container(
    padding: const EdgeInsets.all(32),
    child: Row(
      children: [
        Expanded(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      //OCEAN
                      'Mission Bay',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    //LOCATION
                    'San Diego, California',
                    style: TextStyle(
                      color: Colors.lightBlue[500],
                    ),
                  ),
                ],
                ),
        ),

        //REMOVE THE ICON AND REPLACED WITH FAVORITEWIDGET
        /*
        Icon(
          Icons.favorite,
          color: Colors.redAccent[500],
        ),
        //TEXT FOR AMOUNT OF LOVE THE PETS GIVE AND GET
        Text('text 3'),
      ],
    ),
  );
*/
        FavoriteWidget(),
      ],
      //SET ICON AND COLOR USED FOR LAYOUT
    ),
  );

// DEFINE A SPECIFIC OBJECT _BUILD BUTTON COLUMN
  // INCLUDES A TEXT ITEM AND ICON

  @override
  Widget build(BuildContext context) {
    // BACKGROUND COLOR TYPE IS DEFAULT TO FLUTTER
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        //SPACE EVENLY CAN AUTOMATICALLY ARRANGE ITEMS TO FIT SCOPE
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          // SEARCHED FOR ICONS IN MATERIALS LIBRARY
          //CREATE BUTTONS WITH DESCRIPTORS
          //DESCRIBE SOME OF MY FAVORITE THINGS ABOUT SEA FISHING
          //YOU BECOME A MORE PATIENT
          _buildButtonColumn(color, Icons.timelapse, 'Patience'),
          //YOU TRUST THOSE ON YOUR BOAT
          _buildButtonColumn(color, Icons.group, 'Trust'),
          //YOU MAKE SURE EVERYONE'S SAFETY IS A PRIORITY
          _buildButtonColumn(color, Icons.pool, 'Safety'),
        ],
      ),
    );

// FILL DESCRIPTOR TEXT WITH APPROPRIATE INFORMATION
    // SINGLE QUOTATIONS AND SPACING IS IMPORTANT TO LAYOUT DESIGN
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        // ESSAY TIME...
        'Fishing brings my family together. I grew up eating fish and going on '
            ' fishing trips with my dad and older brothers. The trips in mission bay are'
            ' my favorite. Deep sea fishing is very different than lake or pier fishing.'
            ' Ive learned you must be patient for fish to come and trust the group of '
            ' people on your boat for assistance. Safety is a priority and you must know'
            ' how to swim or wear a life jacket at all times. The waters become very choppy.'
            ' Fishing is an activity that my family can do year-round and catch something new'
            ' each time.',
        // INCLUDE SOFT WRAP
        softWrap: true,
      ),
    );

// CONTROLS MY WIDGETS,BUTTONS,ICONS...ETC
    return MaterialApp(
      //TITLE MY ASSIGNMENT
      title: 'Stateful Object Homework',
      // MATERIAL DESIGN LAYOUT STRUCTURE
      home: Scaffold(
        //CREATES A BAR AT THE TOP OF THE SCREEN
        appBar: AppBar(
          //TITLE OF APP
          title: Text('Deep Sea Fishing'),
        ),
        // ENCAPSULATE THE FOLLOWING CODE WITHIN LIST VIEW
        // ALLOWS SCROLLING FOR SMALLER DEVICES
        body: ListView(
          children: [
            Image.asset(
              // IMPLEMENTS IMAGE FROM DIRECTORY - images/sea.jpg
              'images/sea.jpg',
              //FITS THE IMAGE INTO THE LAYOUT DESIGN
              width: 600,
              height: 240,
              fit: BoxFit.cover,

            ),
            //ENCAPSULATE MY BUTTONS
            titleSection,
            buttonSection,
            textSection,
          ],
        ),
      ),
    );
  }// END OF BUILD

  //STRUCTURE FOR MY BUTTON COLUMN
  // INITIALIZES THE COLOR, ICONS AND NAME FOR ICON
  //MAIN AXIS_SIZE AND MAINAXIS_ALIGNMENT CONTROLS THE SIZE AND POSITION

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ], // END OF CHILDREN
    );
  }// END OF BUILD BUTTON
} //END OF MY APP


//CREATE A FAVORITE WIDGET
class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}
//END OF FAVORITE WIDGET

//FAVORITE WIDGET FIELDS
class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 30;

//DEFINE THE FUNCTION
  //BOOLEAN FUNCTION WITH INCREMENTER & DECREMENTAL
  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            alignment: Alignment.centerRight,
            //CREATE TOGGLE KEYS FOR INTERACTIVE WIDGET
            icon: (_isFavorited ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.lightBlue[500],
            onPressed: _toggleFavorite,
          )
          ,
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
    //-FAVORITEWIDGETSTATE
  }
}
